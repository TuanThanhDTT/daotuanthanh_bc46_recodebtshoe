//rafce
import React from 'react'
import Parent from './Parent'

const DemoProps = () => {
    return (
        <div className="container mt-5">
            <h1>DemoProps</h1>
            <Parent value={1} bgColor="red" fs="100" height={10} />
            <Parent value={2} />
            <Parent value={3} />
            <Parent value={4} />
        </div>
    )
}

export default DemoProps
