import React from 'react'
import ProductItem from './ProductItem'

const ShoeList = (props) => {
    const {data, handlePrdDetail, handleCartShoe} = props
  return (
    <div>
        <div className="row">
            {
                data.map((arr_item) => {
                    return <ProductItem arr_item={arr_item} key={arr_item.id} handlePrdDetail={handlePrdDetail} handleCartShoe={handleCartShoe}/>
                })
            }
        </div>
    </div>
  )
}

export default ShoeList