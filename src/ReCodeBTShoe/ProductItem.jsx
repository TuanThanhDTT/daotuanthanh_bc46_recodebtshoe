import React from 'react'

const ProductItem = (props) => {
    const {arr_item, handlePrdDetail, handleCartShoe} = props

  return (
    <div className='col-4 mt-3'>
        <div className="card">
            <img src={arr_item.image} alt="" />
            <div className="card-body">
                <p className="font-weight-bold">{arr_item.name}</p>
                <p>{arr_item.price} $</p>
                <button className="btn btn-outline-success mt-3" data-toggle="modal" data-target="#DetailShoeModal" onClick={()=> handlePrdDetail(arr_item)}>Xem chi tiết</button>
                <button className='btn btn-success mt-3 ml-5' onClick={()=>handleCartShoe(arr_item)}>Mua sản phẩm</button>
            </div>
               
            </div>
        </div>
  )
}

export default ProductItem