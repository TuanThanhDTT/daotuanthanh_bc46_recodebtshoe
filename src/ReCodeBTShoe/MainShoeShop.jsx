import React, { useState } from 'react'
import ShoeList from './ShoeList'
import data from './data.json'
import CartShoe from '../BTShoes/CartShoe'
const MainShoeShop = () => {
    // Chi tiết sản phẩm
    const [prdDetail, setPrdDetail] = useState(data[0])
    const handlePrdDetail = (product) => {
        setPrdDetail(product)
    }

    //Giỏ hàng sản phẩm
    const [cartShoe, setCartShoe] = useState([])
    const handleCartShoe = (product) => {
        //Dùng hàm Find duyệt mảng kiểm tra xem sản phẩm có trong cart hay chưa
        const item = cartShoe.find((prd) => prd.id === product.id)

        if(!item){
            //Ko tồn tại trong carts thì clone lại object là product và thêm một key soluong với value = 1
            const newPrd = {...product, soluong: 1}
            setCartShoe([...cartShoe, newPrd])
        } else {
            //Có tồn tại trong carts thì clone lại thuộc tính của object sau đó trỏ tới key soluong đã thêm trước đó và gán lại value bằng soluong: item.soluong + 1
            const newPrd = {...item, soluong: item.soluong + 1}
            //Tạo ra môt mảng mới ko chứa sản phẩm có id = product.id truyền vào
            const newCart= cartShoe.filter(cartItem => cartItem.id !== item.id)
            setCartShoe([...newCart, newPrd])
        }
    }

    //Tăng giảm quantity sản phẩm
    const handleQuantity = (idSP, quantity) => {
        setCartShoe((currentState) => {
            return currentState.map(cart => {
                if(cart.id === idSP){
                    return {
                        ...cart,
                        // soluong: cart.soluong + quantity <= 1 ? 1 : cart.soluong + quantity,
                        // Cách 2: nếu true thì lấy vế 1, false thì vế 2
                        soluong: cart.soluong + quantity || 1,
                    }
                }
                return cart
            })
        })
    }
  return (
    <div className="container mt-5">
        <div className='d-flex justify-content-between'>
            <h1 className='text-center'>SHOE SHOP</h1>
            <button className='btn btn-warning' data-toggle="modal" data-target="#cartShoeShop">Cart</button>
        </div>
    
        <ShoeList data={data} handlePrdDetail={handlePrdDetail} handleCartShoe={handleCartShoe} />
        <CartShoe cartShoe={cartShoe} handleQuantity={handleQuantity}/>
            {/* Modal */}

            <div>
                <div
                    className="modal fade"
                    id="DetailShoeModal"
                    tabIndex={-1}
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">
                                    Chi tiết sản phẩm
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-4">
                                        <img
                                            className="img-fluid"
                                            src={prdDetail.image}
                                            alt="..."
                                        />
                                    </div>
                                    <div className="col-8">
                                        <p className="font-weight-bold">{prdDetail.name}</p>
                                        <p className="mt-3">{prdDetail.description}</p>
                                        <p className="font-weight-bold mt-3">{prdDetail.price} $</p>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                >
                                    Close
                                </button>
                                <button type="button" className="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
  )
}

export default MainShoeShop