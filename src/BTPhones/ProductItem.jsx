import React from 'react'

const ProductItem = ({ product, handlePrdDetail, handleCart }) => {
    // console.log('products: ', product)
    // const { product } = props
    return (
        <div className="col-4">
            <div className="card">
                <div>
                    <img
                        style={{ width: 350, height: 350 }}
                        className="img-fluid"
                        src={product.hinhAnh}
                        alt="..."
                    />
                </div>
                <div className="card-body">
                    <p>
                        <b>{product.tenSP}</b>
                    </p>
                    <button
                        className="btn btn-success mt-3"
                        onClick={() => handlePrdDetail(product)}
                    >
                        Xem chi tiết
                    </button>
                    <button
                        className="btn btn-warning mt-3 ml-3"
                        onClick={() => handleCart(product)}
                    >
                        Thêm giỏ hàng
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ProductItem
