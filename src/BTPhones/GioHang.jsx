import React from 'react'

const GioHang = ({ carts, handleQuantity }) => {
    return (
        <div>
            <div
                className="modal fade"
                id="gioHang"
                tabIndex={-1}
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog modal-xl">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Giỏ hàng
                            </h5>
                            <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên SP</th>
                                        <th>Hình ảnh</th>
                                        <th>Giá tiền</th>
                                        <th>Số lượng</th>
                                        <th>Tổng tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {carts.map((cart, index) => (
                                        <tr key={cart.maSP}>
                                            <td>{index + 1}</td>
                                            <td>{cart.tenSP}</td>
                                            <td>
                                                <img
                                                    style={{ width: 100, height: 100 }}
                                                    src={cart.hinhAnh}
                                                    alt="..."
                                                />
                                            </td>
                                            <td>{(cart.giaBan).toLocaleString()}</td>
                                            <td>
                                                <div>
                                                    <button className="btn btn-danger" onClick={() => handleQuantity(cart.maSP, -1)}>-</button>
                                                    {' '}{cart.soluong}{' '}
                                                    <button className="btn btn-success" onClick={() => handleQuantity(cart.maSP, 1)}>+</button>
                                                </div>
                                            </td>
                                            <td>{(cart.soluong * cart.giaBan).toLocaleString()}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-dismiss="modal"
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GioHang
