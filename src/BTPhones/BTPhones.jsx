//rafce
import React, { useState } from 'react'
import ProductList from './ProductList'
import data from './data.json'
import ProductDetail from './ProductDetail'
import GioHang from './GioHang'

const BTPhones = () => {
    // chi tiết sản phẩm
    const [prdDetail, setPrdDetail] = useState(data[0])

    // Danh sách giỏ hàng
    const [carts, setCarts] = useState([
        // {
        //     maSP: 2,
        //     tenSP: 'Meizu 16Xs',
        //     manHinh: 'AMOLED, FHD+ 2232 x 1080 pixels',
        //     heDieuHanh: 'Android 9.0 (Pie); Flyme',
        //     cameraTruoc: '20 MP',
        //     cameraSau: 'Chính 48 MP & Phụ 8 MP, 5 MP',
        //     ram: '4 GB',
        //     rom: '64 GB',
        //     giaBan: 7600000,
        //     hinhAnh: './images/phones/meizuphone.jpg',
        //     soLuong: 4,
        // },
    ])

    const handlePrdDetail = (product) => {
        setPrdDetail(product)
    }

    const handleCart = (product) => {
        // coppy những sản phẩm đang có ở trong cart và thêm vào 1 product mới
        // spread operator

        //Kiểm tra xem sản phẩm có trong giỏ hàng hay chưa
        //Find => tìm thấy trả về đúng item thỏa mãn đk trong mảng, ko tìm thấy trả về undefined
        const item = carts.find((prd) => prd.maSP == product.maSP)

        if(!item){
            //Ko tồn tại trong carts
            const newPrd = {...product, soluong: 1}
            setCarts([...carts, newPrd])
        } else {
            //Có tồn tại trong carts
            const newPrd = {...item, soluong: item.soluong + 1}
            //Tạo ra môt mảng mới ko chứa sản phẩm có maSP = product.maSP truyền vào
            const newCart= carts.filter(cart => cart.maSP !== item.maSP)
            setCarts([...newCart, newPrd])
        }
    
    }

    const handleQuantity = (maSP, quantity) => {
        setCarts((currentState) => {
            return currentState.map(cart => {
                if(cart.maSP == maSP){
                    return {
                        ...cart,
                        // soluong: cart.soluong + quantity <= 1 ? 1 : cart.soluong + quantity,
                        // Cách 2: nếu true thì lấy vế 1, false thì vế 2
                        soluong: cart.soluong + quantity || 1,
                    }
                }
                return cart
            })
        })
    }


    return (
        <div className="container mt-5">
            <div className="d-flex justify-content-between mb-5">
                <h1>BTPhones</h1>
                <button className="btn btn-danger" data-toggle="modal" data-target="#gioHang">
                    Giỏ hàng
                </button>
            </div>

            <ProductList data={data} handlePrdDetail={handlePrdDetail} handleCart={handleCart} />

            {/* Chi tiết sản phẩm */}
            <ProductDetail prdDetail={prdDetail} />

            {/* Giỏ hàng */}
            <GioHang carts={carts} handleQuantity={handleQuantity}/>
        </div>
    )
}

export default BTPhones
