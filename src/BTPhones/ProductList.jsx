import React from 'react'
import ProductItem from './ProductItem'

const ProductList = (props) => {
    const { data, handlePrdDetail, handleCart } = props

    return (
        <div className="row">
            {data.map((product) => (
                <ProductItem
                    product={product}
                    key={product.maSP}
                    handlePrdDetail={handlePrdDetail}
                    handleCart={handleCart}
                />
            ))}
        </div>
    )
}

export default ProductList
