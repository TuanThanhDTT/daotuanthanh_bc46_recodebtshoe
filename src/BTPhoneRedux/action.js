import { HANDLE_CARTS, HANDLE_PRODUCTDETAIL } from "./actionType"

export const productDetailAction = (payload) => {
    return {
        type: HANDLE_PRODUCTDETAIL,
        payload,
    }
}

export const cartAction = (payload) => {
    return {
        type: HANDLE_CARTS,
        payload,
    }
}