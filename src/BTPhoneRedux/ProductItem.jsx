import React from 'react'
import { useDispatch } from 'react-redux'
import { cartAction, productDetailAction } from './action'

const ProductItem = ({ product }) => {
    // console.log('products: ', product)
    // const { product } = props
    const dispatch = useDispatch()
    return (
        <div className="col-4">
            <div className="card">
                <div>
                    <img
                        style={{ width: 350, height: 350 }}
                        className="img-fluid"
                        src={product.hinhAnh}
                        alt="..."
                    />
                </div>
                <div className="card-body">
                    <p>
                        <b>{product.tenSP}</b>
                    </p>
                    <button
                        className="btn btn-success mt-3" onClick={() => {
                            // dispatch({
                            //     type: 'HANDLE_PRODUCTDETAIL',
                            //     payload: product,
                            // })
                            dispatch(productDetailAction(product))
                        }}
                    >
                        Xem chi tiết
                    </button>
                    <button
                        className="btn btn-warning mt-3 ml-3"
                        onClick={() => {
                            // dispatch({
                            //     type: "HANDLE_CARTS",
                            //     payload: product,
                            // })
                            dispatch(cartAction(product))
                        }}
                    >
                        Thêm giỏ hàng
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ProductItem
