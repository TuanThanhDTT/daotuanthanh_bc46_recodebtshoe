import React from 'react'
import data from './data.json'
import ProductList from './ProductList'
import ProductDetail from './ProductDetail'
import GioHang from './GioHang'
const BTPhone = () => {
  return (
    <div className='container mt-5'>
        <div className='d-flex justify-content-between'>
            <h1>BTPhone</h1>
            <button className='btn btn-success' data-toggle="modal" data-target="#gioHangRedux">Giỏ hàng</button>
        </div>
            <ProductList data={data}/>
            <ProductDetail/>
            <GioHang/>
    </div>
  )
}

export default BTPhone