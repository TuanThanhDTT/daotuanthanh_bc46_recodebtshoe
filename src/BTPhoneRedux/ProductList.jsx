import React from 'react'
import ProductItem from './ProductItem'

const ProductList = ({data}) => {
  return (
    <div className='row'>
        {
            data.map((product) => (
                <ProductItem key={product.maSP} product={product} />
            ))
        }
    </div>
  )
}

export default ProductList