import React, { useState } from 'react'
import ListProduct from './ListProduct'
import data from './data.json'

const BTShoes = () => {
    const [prdDetail, setPrdDetail] = useState(data[0])
    console.log('prdDetail: ', prdDetail)

    const handlePrdDetail = (product) => {
        setPrdDetail(product)
    }
    return (
        <div className="container mt-5">
            <h1 className="text-center">Shoe Shop</h1>
            <ListProduct handlePrdDetail={handlePrdDetail} data={data} />

            {/* Modal */}

            <div>
                <div
                    className="modal fade"
                    id="exampleModal"
                    tabIndex={-1}
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">
                                    Chi tiết sản phẩm
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-4">
                                        <img
                                            className="img-fluid"
                                            src={prdDetail.image}
                                            alt="..."
                                        />
                                    </div>
                                    <div className="col-8">
                                        <p className="font-weight-bold">{prdDetail.name}</p>
                                        <p className="mt-3">{prdDetail.description}</p>
                                        <p className="font-weight-bold mt-3">{prdDetail.price} $</p>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                >
                                    Close
                                </button>
                                <button type="button" className="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BTShoes
