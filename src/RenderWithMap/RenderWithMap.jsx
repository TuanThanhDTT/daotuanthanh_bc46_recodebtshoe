// rafce
import React from 'react'

const RenderWithMap = () => {
    const userList = [
        {
            name: 'A',
            age: 16,
            address: 'HCM',
            // id: '123'
        },
        {
            name: 'B',
            age: 17,
            address: 'Hà Nội',
        },
        {
            name: 'C',
            age: 18,
            address: 'Đà Nẵng',
        },
    ]

    const renderUserList = () => {
        return userList.map((user, index) => {
            return (
                <li key={index}>
                    name: {user.name}, age: {user.age}, address: {user.address}
                </li>
            )
        })
    }

    return (
        <div className="container mt-5">
            RenderWithMap
            <h1 className="title">Title Render With Map</h1>
            <h1 className="subTitle">Sub Title</h1>
            <ul>
                {/* 1. Phải có thuộc tính key ở thẻ bao bọc ngoài cùng */}
                {/* 2. Key phải là giá trị duy nhất, không bị trùng lặp */}
                {/* 3. Hạn chế sử dụng index làm giá trị của thuộc tính key, 
                chỉ nên sử dụng khi mảng là mảng tịnh (không thêm sửa xóa gì ở danh sách render) */}

                {/* {userList.map((user, index) => {
                    return (
                        <li key={index}>
                            name: {user.name}, age: {user.age}, address: {user.address}
                        </li>
                    )
                })} */}

                {renderUserList()}
            </ul>
        </div>
    )
}

export default RenderWithMap
