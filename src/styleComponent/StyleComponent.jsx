import React from 'react'

import './style.css'
// => ảnh hưởng tới toàn bộ dự án

import style from './style.module.css'
// => chỉ ảnh hưởng đến component đc sử dụng

import './style.scss'

const StyleComponent = () => {
    return (
        <div className="container mt-5 StyleComponent">
            StyleComponent
            <h1 className="title mt-3">Title</h1>

            {/* style module */}
            <h2 className={style.subTitle}>Sub title</h2>
            <h2 className={style['sub-title']}>Sub title 1</h2>
            <h2 className={`${style['sub-title']} ${style.mt20}`}>Description</h2>

            {/* Style inline */}
            <p style={{ fontWeight: 700, fontSize: '40px' }}>Description ABC</p>

            <button className='button'>Button</button>
        </div>
    )
}

export default StyleComponent
