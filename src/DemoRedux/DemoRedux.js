import React from 'react'
import { useSelector, useDispatch } from 'react-redux'


const DemoRedux = () => {
    //useSelector: dùng để lấy giá trị trên store của redux
    const demoRedux =  useSelector((state) => state.demoRedux)
    // console.log("demoRedux",demoRedux);

    const dispatch = useDispatch()
    //useDispatch thay đổi nội dung state trên store
  return (
   
    <div className='text-center'>
        <h1>DemoRedux</h1>
        <p className='display-4'>Number: {demoRedux.number}</p>
        <p className='display-4'>Name: {demoRedux.name}</p>
        <div>
            <button className='btn btn-success' onClick={() => { dispatch({ 
                type: 'INCREA_NUMBER',
                payload: 2,
        })
        }}>IncreNumber</button>
        </div>
    </div>
  )
}

export default DemoRedux