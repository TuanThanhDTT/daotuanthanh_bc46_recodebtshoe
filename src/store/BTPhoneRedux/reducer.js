const initialState = {
    productDetail:    {
        "maSP": 1,
        "tenSP": "VinSmart Live",
        "manHinh": "AMOLED, 6.2, Full HD+",
        "heDieuHanh": "Android 9.0 (Pie)",
        "cameraTruoc": "20 MP",
        "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP",
        "ram": "4 GB",
        "rom": "64 GB",
        "giaBan": 5700000,
        "hinhAnh": "./images/phones/vsphone.jpg"
    },
    cart: [],

}

export const btPhoneReducer = (state = initialState, action) => {
    console.log('action',action);
    switch (action.type) {
        case 'HANDLE_PRODUCTDETAIL': {
            return {...state, productDetail: {...action.payload}}
        }
        case 'HANDLE_CARTS': {
            const newCart = [...state.cart]
            //Tìm kiếm trong cart xem có sp trùng với sp gửi lên hay không?
            const indexItem = state.cart.findIndex(prd => prd.maSP == action.payload.maSP)

            //Nếu chưa tồn tại sp trong mảng cart
            if(indexItem == -1){
                newCart.push({
                   ...action.payload,
                    soluong:1,
                })
            } else {
                newCart[indexItem].soluong += 1
            }
            return {...state, cart: newCart}
        }
        case 'HANDLELQUANTITY':{
            let newCart = [...state.cart]
            newCart = newCart.map(cart => {
                if(cart.maSP == action.payload.maSP){
                    return{
                        ...cart,
                        soluong: (cart.soluong += action.payload.quantity) || 1,
                    }
                }
                return cart
            })
            return {...state, cart: newCart}
        }

        default:
           return state
    }
}