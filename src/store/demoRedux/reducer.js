const initialState = {
    number : 1,
    name: "ABC"
}

const demoReduxReducer = (state = initialState, action) => {
    // console.log("action",action);
    switch (action.type) {
        case "INCREA_NUMBER": {
            const data = {...state}
            data.number += action.payload
            return data
        }
        default:
            return state
    }
    
}

export default demoReduxReducer