import { combineReducers } from 'redux';

import demoReduxReducer from './demoRedux/reducer'
import { btPhoneReducer } from './BTPhoneRedux/reducer';
export const rootReducer = combineReducers({
    demoRedux: demoReduxReducer,
    btPhone: btPhoneReducer,
})
