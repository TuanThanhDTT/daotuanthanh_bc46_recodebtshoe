// import logo from './logo.svg'
// import DemoFunctionComponent from './components/DemoFunctionComponent'
// import DemoClassComponent from './components/DemoClassComponent'
import './App.css'
import MainShoeShop from './ReCodeBTShoe/MainShoeShop'
// import BTPhone from './BTPhoneRedux/BTPhone'
// import BTPhones from './BTPhones/BTPhones'
// import DemoRedux from './DemoRedux/DemoRedux'
// JSX: Javascript XML => cho phép các bạn viết html 1 cách dễ dàng trong js
// binding:
// attributes: viết theo quy tắc camelCase

// Component: thành phần nhỏ giao diện ứng dụng
// 2 loại component: function component (stateLess component), class component (stateFull component) lifecicle

function App() {
    return (
        <div>
            <MainShoeShop/>
            {/* <DemoFunctionComponent />

            <DemoFunctionComponent></DemoFunctionComponent>

            <DemoClassComponent /> */}

            {/* BT component */}
            {/* <Home /> */}

            {/* <BTPhones />
            <DemoRedux/>
            <BTPhone/> */}
        </div>
    )
}

export default App
